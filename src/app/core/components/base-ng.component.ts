import { Component, ChangeDetectorRef, Injector, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseUIState } from '../models/base-ui-state.model';
import { IBaseNg, AbstractBaseNgComponent } from '../models/base-ng.interface';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import moment from 'moment-mini';

@Component({
    selector: 'app-base-ng',
    template: '',
    styleUrls: []
  })
export class BaseNgComponent<T extends BaseUIState = BaseUIState> extends AbstractBaseNgComponent implements IBaseNg, OnInit, OnDestroy {

    constructor(protected injector?: Injector) {
        super();
        this.state = new BaseUIState();
        this.route = this.injector.get(ActivatedRoute);
        this.router = this.injector.get(Router);
        this.cdRef = this.injector.get(ChangeDetectorRef);
        this.http = injector.get(HttpClient);
        this.math = Math;
        this.moment = moment;
        this.of = of;
        this.object = Object;
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }
}

