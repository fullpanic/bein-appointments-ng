import { NgModule } from '@angular/core';
import { BaseNgComponent } from './components/base-ng.component';

@NgModule({
  declarations: [BaseNgComponent],
  exports: [BaseNgComponent]
})
export class CoreModule { }
