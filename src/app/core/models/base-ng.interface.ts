import { ActivatedRoute, Router } from "@angular/router";
import { ChangeDetectorRef } from "@angular/core";
import { BaseUIState } from "./base-ui-state.model";
import { HttpClient } from "@angular/common/http";


export interface IBaseNg<T extends BaseUIState = BaseUIState> {
    /**
     * To access url, get & fragment params
     */
    route: ActivatedRoute;

    /**
     * Angular router utility
     */
    router: Router;

    /**
     * Change detection reference
     */
    cdRef: ChangeDetectorRef;

    /**
     * Math object accessor
     */
    math: Math;

    /**
     * MomentJS
     */
    moment: any;

    /**
     * Object accessor
     */
    object: any;

    /**
     * Observable helper
     */
    of: any;

    /**
     * Ui state
     */
    state: T;
}

export abstract class AbstractBaseNgComponent<T extends BaseUIState = BaseUIState> implements IBaseNg {
    /**
     * To access url, get & fragment params
     */
    route: ActivatedRoute;

    /**
     * Angular router utility
     */
    router: Router;

    /**
     * Http client to fetch data
     */
    http: HttpClient;

    /**
     * Change detection reference
     */
    cdRef: ChangeDetectorRef;

    /**
     * Math object accessor
     */
    math: Math;

    /**
     * MomentJS
     */
    moment: any;

    /**
     * Object accessor
     */
    object: any;

    /**
     * Observable helper
     */
    of: any;

    /**
     * Ui state
     */
    state: T;
}
