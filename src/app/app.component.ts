import { Component, Injector } from '@angular/core';
import { BaseNgComponent } from './core/components/base-ng.component';
import { TimezoneService } from './shared/services/timezone.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseNgComponent{

  timezone: TimezoneService;

  constructor(protected injector: Injector) {
    super(injector);
    this.timezone = injector.get(TimezoneService);
  }

  timezones = {
    'america/new_york': 'america/new_york (GMT-4)',
    'europe/lisbon': 'europe/lisbon (GMT+1)',
    'europe/moscow': 'europe/moscow (GMT+3)',
    'asia/calcutta': 'asia/calcutta (GMT+5:30)',
    'asia/shanghai': 'asia/shanghai (GMT+8)',
    'australia/sydney': 'australia/sydney (GMT+10)',
    'pacific/auckland': 'pacific/auckland (GMT+12)'
  };

  timezoneSelected($event) {
    this.timezone.set($event.target.value);
  }
}
