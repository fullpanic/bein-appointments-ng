import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'experts',
    loadChildren: () => import('./modules/experts/experts.module').then(m => m.ExpertsModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'experts'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
