import { Component, Injector, OnInit } from '@angular/core';
import { BaseNgComponent } from '../../../core/components/base-ng.component';
import { ExpertService } from '../../../shared/services/expert.service';
import { PaginationRequest } from '../../../shared/models/dto/request/pagination-request.model';
import { finalize } from 'rxjs/operators';
import { ExpertModel } from '../../../shared/models/dto/response/expert.model';

@Component({
  selector: 'app-expert-list',
  templateUrl: './expert-list.component.html',
  styleUrls: ['./expert-list.component.scss']
})
export class ExpertListComponent extends BaseNgComponent implements OnInit {

  expert: ExpertService;
  list: ExpertModel[];

  constructor(protected injector: Injector) {
    super(injector);
    this.expert = injector.get(ExpertService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.state.setIsLoading();
    this.expert.paginate(new PaginationRequest())
      .pipe(finalize(() => this.state.setIsLoading(false)))
      .subscribe(res => {
        this.list = res.data;
      });
  }

}
