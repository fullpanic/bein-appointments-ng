import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExpertListComponent } from './expert-list/expert-list.component';
import { ExpertDetailsComponent } from './expert-details/expert-details.component';
import { AppointmentFormComponent } from './appointment-form/appointment-form.component';


const routes: Routes = [
  {
    path: '',
    component: ExpertListComponent,
  },
  {
      path: ':id',
      children: [
          {
              path: '',
              component: ExpertDetailsComponent
          },
          {
              path: 'book',
              component: AppointmentFormComponent,
          }
      ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpertsRoutingModule { }
