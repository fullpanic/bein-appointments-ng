import { Component, Injector, OnInit } from '@angular/core';
import { BaseNgComponent } from '../../../core/components/base-ng.component';
import { ExpertService } from '../../../shared/services/expert.service';
import { PaginationRequest } from '../../../shared/models/dto/request/pagination-request.model';
import { finalize } from 'rxjs/operators';
import { ExpertModel } from '../../../shared/models/dto/response/expert.model';
import { TimezoneService } from '../../../shared/services/timezone.service';
import * as moment from 'moment';
import { DateFormat } from '../../../shared/models/enums/date-format.enum';

@Component({
  selector: 'app-expert-details',
  templateUrl: './expert-details.component.html',
  styleUrls: ['./expert-details.component.scss']
})
export class ExpertDetailsComponent extends BaseNgComponent implements OnInit {

  timezone: TimezoneService;
  expert: ExpertService;
  dataSource: ExpertModel;

  constructor(protected injector: Injector) {
    super(injector);
    this.expert = injector.get(ExpertService);
    this.timezone = injector.get(TimezoneService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.timezone.change$.subscribe(tz => this.load());
  }

  load() {
    this.state.setIsLoading();
    this.expert.find(+this.route.snapshot.paramMap.get('id'))
      .pipe(finalize(() => this.state.setIsLoading(false)))
      .subscribe(res => {
        this.dataSource = res;
      });
  }

  getTimeBase12(time: string) {
    return moment(time, DateFormat.TimeShort).format('h:mm A');
  }

}