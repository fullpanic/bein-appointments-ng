import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BaseNgComponent } from '../../../core/components/base-ng.component';
import { ExpertService } from '../../../shared/services/expert.service';
import { PaginationRequest } from '../../../shared/models/dto/request/pagination-request.model';
import { finalize } from 'rxjs/operators';
import { AppointmentModel } from '../../../shared/models/dto/response/appointment.model';
import { ExpertModel } from '../../../shared/models/dto/response/expert.model';
import { TimezoneService } from '../../../shared/services/timezone.service';
import { merge, Subscription, forkJoin } from 'rxjs';
import * as moment from 'moment';
import { DateFormat } from '../../../shared/models/enums/date-format.enum';

interface TimeSlot {
  label: string;
  value: { from: string, to: string };
}

@Component({
  selector: 'app-appointment-form',
  templateUrl: './appointment-form.component.html',
  styleUrls: ['./appointment-form.component.scss']
})
export class AppointmentFormComponent extends BaseNgComponent implements OnInit {

  timezone: TimezoneService;

  expert: ExpertService;

  dataSource: ExpertModel;

  appointments: AppointmentModel[] = [];

  timeSlots: Array<TimeSlot> = [];

  form = new FormGroup({
    date: new FormControl(null, [Validators.required]),
    duration: new FormControl(15, [Validators.required]),
    timeSlot: new FormControl(null, [Validators.required])
  });

  subscription: Subscription;

  errors: any;

  success: AppointmentModel;

  constructor(protected injector: Injector) {
    super(injector);
    this.expert = injector.get(ExpertService);
    this.timezone = injector.get(TimezoneService);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.timezone.change$.subscribe(tz => this.load());
  }

  load(): void {
    this.state.setIsLoading();
    const expertId = +this.route.snapshot.paramMap.get('id');
    forkJoin(this.expert.find(expertId), this.expert.appointments(expertId, '1900-01-01 00:00:00', '2100-01-01 00:00:00'))
      .pipe(finalize(() => this.state.setIsLoading(false)))
      .subscribe(res => {
        this.dataSource = res[0];
        this.appointments = res[1];
        if (this.subscription) {
          this.subscription.unsubscribe();
        }
        this.subscribeChanges();
        this.resetForm();
      });
  }

  resetForm() {
    this.timeSlots = [];
    this.errors = null;
    this.form.get('date').setValue(null);
    this.form.get('timeSlot').setValue(null);
  }

  makeSlot(from: moment.Moment, to: moment.Moment) {
    return {
      label: `${from.format('h:mm A')} - ${to.format('h:mm A')}`,
      value: { from, to }
    };
  }

  subscribeChanges(): void {
    this.subscription = merge(this.form.get('duration').valueChanges, this.form.get('date').valueChanges)
      .subscribe(_ => {
        this.form.get('timeSlot').setValue(null);
        this.updateTimeSlots();
      });
  }

  generateSlots(start: string, end: string): TimeSlot[] {
    debugger;
    const date = moment(this.form.get('date').value).format(DateFormat.Date);
    const duration = this.form.get('duration').value;
    let timeSlots = [];
    let from = moment(`${date} ${start}:00`, DateFormat.Datetime);
    while (true) {
      let to = moment(from)
      to.add(duration, 'minutes');
      if (to.format(DateFormat.TimeShort) > end || to.format(DateFormat.TimeShort) === '00:00') {
        break;
      }
      let ok = true;
      for (const appointment of this.appointments) {
        if ((from.isAfter(appointment.from) && from.isBefore(appointment.to)) ||
            (to.isAfter(appointment.from) && to.isBefore(appointment.to)) ||
            (appointment.from.isSameOrAfter(from) && appointment.to.isSameOrBefore(to))) {
              ok = false;
              break;
        }
      }
      if (ok) {
        timeSlots.push(this.makeSlot(moment(from), to));
      }
      from.add(15, 'minutes');
    }
    return timeSlots;
  }

  updateTimeSlots(): void {
    const workStart = this.dataSource.working_hours_start;
    const workEnd = this.dataSource.working_hours_end;
    if (workStart < workEnd) {
        this.timeSlots = this.generateSlots(workStart, workEnd);
    } else {
        this.timeSlots = [
          ...this.generateSlots(workStart, '23:59'),
          ...this.generateSlots('00:00', workEnd)
        ];
    }
  }

submit(): void {
  const expertId = +this.route.snapshot.paramMap.get('id');
  const from = this.form.get('timeSlot').value.from.format(DateFormat.Datetime);
  const to = this.form.get('timeSlot').value.to.format(DateFormat.Datetime);
  this.state.setIsLoading()
  this.expert.book(expertId, from, to)
    .pipe(finalize(() => this.state.setIsLoading(false)))
    .subscribe((response) => {
        this.success = response;
    }, (response) => {
      this.errors = response.error;
    });
  }

}
