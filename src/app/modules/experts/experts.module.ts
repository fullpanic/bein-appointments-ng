import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ExpertsRoutingModule } from './experts-routing.module';
import { ExpertListComponent } from './expert-list/expert-list.component';
import { ExpertDetailsComponent } from './expert-details/expert-details.component';
import { AppointmentFormComponent } from './appointment-form/appointment-form.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [ExpertListComponent, ExpertDetailsComponent, AppointmentFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExpertsRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ]
})
export class ExpertsModule { }
