import { Injectable } from '@angular/core';
import {
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs';
import moment from 'moment-timezone';
import { TimezoneService } from '../services/timezone.service';

@Injectable({
    providedIn: 'root'
})
export class TimezoneInterceptor implements HttpInterceptor {

    constructor(private timezone: TimezoneService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                'X-Timezone': this.timezone.get() || moment.tz.guess(),
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        });
        return next.handle(request);
    }
}
