import { Observable } from "rxjs";
import { ObjectResponse } from "../models/response/object-response.model";

export function extractData<T>(source: Observable<ObjectResponse<T>>): Observable<T> {
    return new Observable(observer => {
        source.subscribe({
            next(value) {
                observer.next(value.data);
            },
            error(err: any) {
                observer.error(err);
            },
            complete() {
                observer.complete();
            }
        })
    });
}
