import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from '../../config/api';
import { PaginationRequest } from '../models/dto/request/pagination-request.model';
import { PaginationResponse } from '../models/response/pagination-response.model';
import { ExpertModel } from '../models/dto/response/expert.model';
import { ObjectResponse } from '../models/response/object-response.model';
import { ListResponse } from '../models/response/list-response.model';
import { extractData } from '../operators/extract-data.operator';
import { AppointmentModel } from '../models/dto/response/appointment.model';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ExpertService {

    private baseEndpoint = API.experts;

    constructor(private http: HttpClient) {
    }

    paginate(request: PaginationRequest): Observable<PaginationResponse<ExpertModel>> {
        const url = `${this.baseEndpoint}?${request.queryParams()}`;
        return this.http.get(url)
          .pipe(
            map(response => new PaginationResponse(response, ExpertModel))
          );
    }

    find(id: number): Observable<ExpertModel> {
      const url = `${this.baseEndpoint}/${id}`;
      return this.http.get(url)
          .pipe(
              map(response => new ObjectResponse(response, ExpertModel)),
              extractData
          );
    }

    appointments(expertId: number, from: string, to: string): Observable<AppointmentModel[]> {
        const url = `${this.baseEndpoint}/${expertId}/appointments?from=${from}&to=${to}`;
        return this.http.get(url)
          .pipe(
            map(response => new ListResponse(response, AppointmentModel)),
            extractData
          );
    }

    book(expertId: number, from: string, to: string): Observable<AppointmentModel> {
        const url = `${this.baseEndpoint}/${expertId}/book`;
        return this.http.post(url, { from, to })
          .pipe(
            map(response => new ObjectResponse(response, AppointmentModel)),
            extractData
          );
    }
}
