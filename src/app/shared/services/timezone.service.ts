import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimezoneService {

  private timezone: string = null;

  change$ = new BehaviorSubject<string>(null);

  constructor() { }

  set(tz: string): void {
    this.timezone = tz;
    this.change$.next(tz);
  }

  get(): string {
    return this.timezone;
  }
}
