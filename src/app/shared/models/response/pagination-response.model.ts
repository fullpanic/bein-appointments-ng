import { BaseModel } from '../dto/base/base.model';

class PaginationMeta extends BaseModel{
    current_page: number;
    from: number;
    to: number;
    last_page: number;
    path: string;
    per_page: number;
    total: number;
}

class PaginationLinks extends BaseModel{
    first: string;
    last: string;
    prev: string;
    next: string;
}

export class PaginationResponse<T> extends BaseModel {
    data: T[];
    meta: PaginationMeta;
    links: PaginationLinks;

    /**
     *
     * @param obj
     * @param constructor `data` constructor passed at runtime.
     */
    constructor(obj: any, constructor: new (...args) => T) {
        super(obj);
        this.data = this.data.map(value => new constructor(value));
        this.meta = new PaginationMeta(this.meta);
        this.links = new PaginationLinks(this.links);
    }
}
