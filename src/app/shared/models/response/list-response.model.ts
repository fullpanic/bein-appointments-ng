import { BaseModel } from '../dto/base/base.model';

export class ListResponse<T> extends BaseModel{
    data: T[];
    message?: string;
    errors?: any;

    /**
     *
     * @param obj
     * @param constructor `data` constructor passed at runtime.
     */
    constructor(obj: any, constructor?: new (...args) => T) {
        super(obj);
        this.data = this.data.map(value => new constructor(value));
    }
}
