import { BaseModel } from '../dto/base/base.model';

export class ObjectResponse<T> extends BaseModel {
    data: T;
    message?: string;
    errors?: any;

    /**
     *
     * @param obj
     * @param constructor `data` constructor passed at runtime.
     */
    constructor(obj: any, constructor?: new (...args) => T) {
        super(obj);
        if (this.data && constructor) {
            this.data = new constructor(this.data);
        } else {
            this.data = obj.data;
        }
    }
}
