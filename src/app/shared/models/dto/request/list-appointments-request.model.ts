export class ListAppointmentsRequest {

    constructor(
        public expert_id: number,
        public from: string,
        public to: string
        ) {
    }
}
