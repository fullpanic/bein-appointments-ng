export class BaseModel {
    id: number;

    /**
     * Constructor takes a raw object and uses it to fill defined object properties.
     * Override to handle construction of nested objects.
     * @param obj
     */
    constructor(obj: any) {
        for (const key of Object.keys(obj)) {
            this[key] = obj[key];
        }
    }
}
