import { BaseModel } from '../base/base.model';

export class ExpertModel extends BaseModel {
    name: string;
    expertise: string;
    country: string;
    timezone: string;
    working_hours_start: string;
    working_hours_end: string;

    constructor(obj: any) {
        super(obj);
    }
}
