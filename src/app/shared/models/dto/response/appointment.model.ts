import { BaseModel } from '../base/base.model';
import { ExpertModel } from './expert.model';
import * as moment from 'moment';
import { DateFormat } from '../../enums/date-format.enum';

export class AppointmentModel extends BaseModel {
    from: moment.Moment;
    to: moment.Moment;
    expert?: ExpertModel;

    constructor(obj: any) {
        super(obj);
        this.from = moment(this.from, DateFormat.Datetime);
        this.to = moment(this.to, DateFormat.Datetime);
        if (this.expert) {
            this.expert = new ExpertModel(this.expert);
        }
    }
}
