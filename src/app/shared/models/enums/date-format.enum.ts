export enum DateFormat {
    Datetime = 'YYYY-MM-DD HH:mm:ss',
    DatetimeShort = 'YYYY-MM-DD HH:mm',
    Date = 'YYYY-MM-DD',
    TimeShort = 'HH:mm',
    Time = 'HH:mm:ss',
    DatetimeZ = 'YYYY-MM-DD HH:mm:ss z',
    DateZ = 'YYYY-MM-DD z',
    TimeShortZ = 'HH:mm z',
    TimeZ = 'HH:mm:ss z',
}
